<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type_document',['cc','ti','tp','rc','ce','ci','dni']);
            $table->string("nit")->unique();
            $table->text('photo')->nullable();
            $table->string('phone');
            $table->string("address");
            $table->unsignedBigInteger('user_id')->unique()->unsigned();
            $table->timestamps();

            //key foreign
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
