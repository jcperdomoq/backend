<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 6/12/18
 * Time: 09:12 PM
 */

return [
    "reset_password" => "Reset Password Notification",
    "reset_message1" => "You are receiving this email because we received a password reset request for your account.",
    "reset_button"  => "Reset Password",
    "reset_message2" => "If you did not request a password reset, no further action is required.",
    "reset_confirm" => "The password was modified",
    "verified" =>"The user is already activated",
    "verifying"=>"The user is enabled"
];