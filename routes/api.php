<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// ruta  inicial, documentacion de la api
Route::get('/', function () {
    return view('welcome');
});

/*
 * rutas de autenticacion
 */
Route::prefix('auth')->group(function(){
    //ruta para el login del usuario
    Route::post('login','AuthApi\AuthController@login');
    //ruta para el registro del usuario
    Route::post('register', 'AuthApi\AuthController@register');
    //ruta para el registro del usuario
    Route::delete('logout', 'AuthApi\AuthController@logout');
    //ruta para  refrescar el token
    Route::get('refresh', 'AuthApi\AuthController@refresh');
    //permite enviar un mensaje de correo para el cambio de contraseña
    Route::post('password-reset','AuthApi\ForgotPasswordController@sendResetLinkEmail');
    //validara token firebase
    Route::get('firebase/{firebase_token}','AuthApi\FirebaseController@login');
});

/*
 * Rutas del perfil del usuario autenticado
 */
Route::prefix('profile')->group(function(){
    // ruta para obtener el usuario autenticado
    Route::get('user', 'Profile\UserController@getAuthenticatedUser');
    // permite actualizar el usuario autenticado
    Route::put('update','Profile\UserController@update');
    // permite modificar la clave del usuario
    Route::put('change-password','Profile\UserController@changePassword');
    // renvia un nuevo mensaje para activar el email
    Route::get('verify-email','AuthApi\VerificationController@resend');
});

// test de brodcast
Route::get('test-broadcast', function(){
    broadcast(new \App\Events\PublicEvent());
    event(new \App\Events\PrivateEvent(auth()->user()->id));
    return response()->json("todo bien");
});