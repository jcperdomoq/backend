@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>@lang('messages.reset_confirm')</h1>
    </div>
@endsection